<?php

namespace Drupal\auto_term_translate\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\auto_node_translate\Form\TranslationForm as NodeTranslationForm;

/**
 * The Translation Form.
 */
class TranslationForm extends NodeTranslationForm {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    LanguageManagerInterface $language_manager,
    ConfigFactoryInterface $config,
    CurrentRouteMatch $route_match,
    TimeInterface $time,
    AccountProxyInterface $current_user,
    ModuleHandlerInterface $module_handler,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($language_manager, $config, $route_match, $time, $current_user, $module_handler);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('language_manager'),
      $container->get('config.factory'),
      $container->get('current_route_match'),
      $container->get('datetime.time'),
      $container->get('current_user'),
      $container->get('module_handler'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'auto_term_translate_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $taxonomy_term = NULL) {
    $languages = $this->languageManager->getLanguages();
    $form['translate'] = [
      '#type' => 'fieldgroup',
      '#title' => $this->t('Languages to Translate'),
      '#closed' => FALSE,
      '#tree' => TRUE,
    ];

    foreach ($languages as $language) {
      $languageId = $language->getId();
      if ($languageId !== $taxonomy_term->langcode->value) {
        $label = ($taxonomy_term->hasTranslation($languageId)) ? $this->t('overwrite translation') : $this->t('new translation');
        $form['translate'][$languageId] = [
          '#type' => 'checkbox',
          '#title' => $this->t('@lang (@label)', [
            '@lang' => $language->getName(),
            '@label' => $label,
          ]),
        ];
      }
    }
    $form['save'] = [
      '#type' => 'submit',
      '#value' => $this->t('Translate'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config->get('auto_node_translate.config');
    if (empty($config->get('default_api'))) {
      $form_state->setError($form['translate'], $this->t('Error, translation API is not configured!'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $term = $this->route->getParameter('taxonomy_term');
    $translations = $form_state->getValues()['translate'];
    $this->autoTaxonomyTranslateTerm($term, $translations);

    $form_state->setRedirect('entity.taxonomy_term.canonical', ['taxonomy_term' => $term->id()]);
  }

  /**
   * Translates Taxonomy Terms.
   *
   * @param \Drupal\taxonomy\Entity\Term $term
   *   The term to translate.
   * @param mixed $translations
   *   The translations array.
   */
  public function autoTaxonomyTranslateTerm(Term $term, $translations) {
    $languageFrom = $term->langcode->value;
    $fields = $term->getFields();
    $excludeFields = $this->getExcludeFields();
    $translatedTypes = $this->getTextFields();
    $config = $this->config->get('auto_node_translate.config');
    $apiType = "Drupal\auto_node_translate\\" . $config->get('default_api');
    $api = new $apiType();

    foreach ($translations as $languageId => $value) {
      if ($value) {
        $termTrans = $this->getTransledTaxonomy($term, $languageId);
        foreach ($fields as $field) {
          $fieldType = $field->getFieldDefinition()->getType();
          $fieldName = $field->getName();

          if (in_array($fieldType, $translatedTypes) && !in_array($fieldName, $excludeFields)) {
            $translatedValue = $this->translateTextField($field, $fieldType, $api, $languageFrom, $languageId);
            $termTrans->set($fieldName, $translatedValue);
          }
          elseif ($fieldType == 'link') {
            $values = $this->translateLinkField($field, $api, $languageFrom, $languageId);
            $termTrans->set($fieldName, $values);
          }
          elseif ($fieldType == 'entity_reference_revisions') {
            // Process later.
          }
          elseif (!in_array($fieldName, $excludeFields)) {
            $termTrans->set($fieldName, $term->get($fieldName)->getValue());
          }
        }
      }
    }

    foreach ($fields as $field) {
      $fieldType = $field->getFieldDefinition()->getType();
      if ($fieldType == 'entity_reference_revisions') {
        $this->translateParagraphField($field, $api, $languageFrom, $translations);
      }
    }

    $term->setNewRevision(TRUE);
    $term->revision_log = $this->t('Automatic translation using @api', ['@api' => $config->get('default_api')]);
    $term->setRevisionCreationTime($this->time->getRequestTime());
    $term->setRevisionUserId($this->currentUser->id());
    $term->save();
  }

  /**
   * Gets or adds translated taxonomy_term.
   *
   * @param mixed $term
   *   The taxonomy_term.
   * @param mixed $languageId
   *   The language id.
   *
   * @return mixed
   *   the translated taxonomy_term.
   */
  public function getTransledTaxonomy(&$term, $languageId) {
    return $term->hasTranslation($languageId) ? $term->getTranslation($languageId) : $term->addTranslation($languageId);
  }

}
